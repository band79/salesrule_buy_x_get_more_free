<?php
/**
 * Extend the Magento core model
 * @author: Andrey Babich, andrey.babich@gmail.com
 */
namespace Band\SalesRule\Model;

use Magento\Quote\Model\Quote\Address;

class Rule extends \Magento\SalesRule\Model\Rule
{
    const BUY_ONE_GET_ANOTHER_FREE_ACTION = 'buy_one_get_another_free';
}