<?php
/**
 * Checkout Price Rule "Buy one, get another for free"
 * @author: Andrey Babich, andrey.babich@gmail.com
 */

namespace Band\SalesRule\Model\Rule\Metadata\ValueProvider;

use Band\SalesRule\Model\Rule;

class Plugin
{
    public function afterGetMetadataValues(\Magento\SalesRule\Model\Rule\Metadata\ValueProvider\Interceptor $rule, $result)
    {
        $applyOptions = [
            ['label' => __('Percent of product price discount'), 'value' =>  Rule::BY_PERCENT_ACTION],
            ['label' => __('Fixed amount discount'), 'value' => Rule::BY_FIXED_ACTION],
            ['label' => __('Fixed amount discount for whole cart'), 'value' => Rule::CART_FIXED_ACTION],
            ['label' => __('Buy X get Y free (discount amount is Y)'), 'value' => Rule::BUY_X_GET_Y_ACTION],
            ['label' => __('Buy One Get Another Free'), 'value' => \Band\SalesRule\Model\Rule::BUY_ONE_GET_ANOTHER_FREE_ACTION]
        ];
        $result['actions']['children']['simple_action']['arguments']['data']['config']['options'] = $applyOptions;
        return $result;
    }
}
