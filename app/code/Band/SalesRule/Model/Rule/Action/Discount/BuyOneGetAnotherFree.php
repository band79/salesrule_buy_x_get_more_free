<?php
/**
 * Checkout Price Rule "Buy one get another for free"
 * @author: Andrey Babich, andrey.babich@gmail.com
 */

namespace Band\SalesRule\Model\Rule\Action\Discount;

class BuyOneGetAnotherFree extends \Magento\SalesRule\Model\Rule\Action\Discount\AbstractDiscount
{
    protected $productRepository;
    protected $cart;
    protected $_ruleApplied = false;

    /**
     * @param \Magento\SalesRule\Model\Validator $validator
     * @param DataFactory $discountDataFactory
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        \Magento\SalesRule\Model\Validator $validator,
        \Magento\SalesRule\Model\Rule\Action\Discount\DataFactory $discountDataFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Cart $cart
    ) {
        $this->validator = $validator;
        $this->discountFactory = $discountDataFactory;
        $this->priceCurrency = $priceCurrency;
        $this->productRepository = $productRepository;
        $this->cart = $cart;
    }

    /**
     * @param \Magento\SalesRule\Model\Rule $rule
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param float $qty
     * @return \Magento\SalesRule\Model\Rule\Action\Discount\Data
     */
    public function calculate($rule, $item, $qty)
    {
        /** @var \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData */
        $discountData = $this->discountFactory->create();
        if (in_array($rule->getId(), explode(',', $item->getAppliedRuleIds()))) {
            // Rule has already applied
            if ($qty === 1) {
                // Only one product in Shopping cart and should
                $qtyApply = $qty;
                $this->_applyDiscount($discountData, $qtyApply, $rule, $item);
                $this->addProductToCart($item);
            } else {
                // Gift product is in Shopping Cart already
                $qtyApply = $qty - 1;
                $this->_applyDiscount($discountData, $qtyApply, $rule, $item);
            }
        } else {
            // Rule has not applied yet
            $qtyApply = $qty;
            $this->_applyDiscount($discountData, $qtyApply, $rule, $item);

            $itemAppliedRuleIds = $item->getAppliedRuleIds();
            $itemAppliedRuleIds .= ($itemAppliedRuleIds ? ',' : ''). $rule->getId();
            $item->setAppliedRuleIds($itemAppliedRuleIds);
            $this->addProductToCart($item);
        }
        return $discountData;
    }

    /**
     * Add product to cart
     *
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param int $qty
     */
    public function addProductToCart($item, $qty = 1)
    {
        try {
            $params = array(
                'product' => $item->getProductId(),
                'qty' => $qty
            );
            $this->cart->addProduct($item->getProduct(), $params);
            $this->cart->save();

            $this->_ruleApplied = true;

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Apply Rule's discount according to rule own discount and gift's price
     *
     * @param \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData
     * @param int $qtyApply
     * @param \Magento\SalesRule\Model\Rule $rule
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     */
    protected function _applyDiscount($discountData, $qtyApply, $rule, $item)
    {
        $quoteAmount = $this->priceCurrency->convert($rule->getDiscountAmount(), $item->getQuote()->getStore());
        $discountData->setAmount($qtyApply * $quoteAmount + $item->getProduct()->getFinalPrice());
        $discountData->setBaseAmount($qtyApply * $rule->getDiscountAmount() + $item->getProduct()->getFinalPrice());
    }
}
