<?php
/**
 * Checkout Price Rule "Buy one, get another for free"
 * @author: Andrey Babich, andrey.babich@gmail.com
 */
/* app/code/Band/SalesRule/Setup/InstallData.php */

namespace Band\SalesRule\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        /**
         * Modify product attribute SKU to use it in Promo Rule Conditions
         */
        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'sku',
            'is_used_for_promo_rules',
            true
        );
    }
}