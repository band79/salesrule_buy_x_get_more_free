<?php
/**
 * Sales Rule module "Buy one get another for free"
 * @author: Andrey Babich, band@ciklum.com
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Band_SalesRule',
    __DIR__
);
